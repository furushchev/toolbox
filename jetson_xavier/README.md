# Use unionfs for Xavier

1. Install NVMe SSD Drive

2. Format SSD Drive as ext4

3. Install scripts

```bash
wget https://raw.githubusercontent.com/furushchev/toolbox/master/jetson_xavier/install.sh -O - | bash -
```
